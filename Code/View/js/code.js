function mouseOverHome1() {
    var x = document.getElementById("home1");

    x.style.color = "turquoise";
}

function mouseLeaveHome1() {
    var x = document.getElementById("home1");

    x.style.color = "white";
}

function mouseOverProfil() {
    var x = document.getElementById("profil");

    x.style.color = "turquoise";
}

function mouseLeaveProfil() {
    var x = document.getElementById("profil");

    x.style.color = "white";
}

function mouseOverLogout() {
    var x = document.getElementById("logout");

    x.style.color = "turquoise";
}

function mouseLeaveLogout() {
    var x = document.getElementById("logout");

    x.style.color = "white";
}

function mouseOverCreateRecipe() {
    var x = document.getElementById("createRecipe");

    x.style.color = "turquoise";
}

function mouseLeaveCreateRecipe() {
    var x = document.getElementById("createRecipe");

    x.style.color = "white";
}

function mouseOverSearch() {
    var x = document.getElementById("search");

    x.style.color = "turquoise";
}

function mouseLeaveSearch() {
    var x = document.getElementById("search");

    x.style.color = "white";
}

function mouseOverSavedPlates() {
    var x = document.getElementById("savedPlates");

    x.style.color = "turquoise";
}

function mouseLeaveSavedPlates() {
    var x = document.getElementById("savedPlates");

    x.style.color = "black";
}

function mouseOverUsers() {
    var x = document.getElementById("users");

    x.style.color = "turquoise";
}

function mouseLeaveUsers() {
    var x = document.getElementById("users");

    x.style.color = "black";
}

function mouseOverConfirmRecipes() {
    var x = document.getElementById("confirmRecipes");

    x.style.color = "turquoise";
}

function mouseLeaveConfirmRecipes() {
    var x = document.getElementById("confirmRecipes");

    x.style.color = "black";
}

function mouseOverRecipe() {
    var x = document.getElementById("recipe");

    x.style.backgroundColor = "lightgrey";
}

function mouseLeaveRecipe() {
    var x = document.getElementById("recipe");

    x.style.backgroundColor = "white";
}




