<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.02.2022
 * Time: 14:44
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <link rel="icon" type="image/png" href="view/css/image/logo.png"/>
    <meta charset="utf-8">
    <title><?= $titre; ?></title>
    <link rel="stylesheet" href="view/css/style.css">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/mdb.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/style.css">
    <style>
        html, body {
            margin: 0;
            height: 100%;
        }
    </style>
</head>
<body>
    <script src="view/js/code.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/mdb.min.js"></script>

    <script>
        $(document).ready(function () {
            new WOW().init();
        });
    </script>

    <div id="home">

        <div class="container-fluid">
            <header>
                <nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
                    <a class="navbar-brand" href="index.php?action=home"><img src="view/css/image/logo.png"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse text-center" id="navbarResponsive">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item
                        <?php if ($_GET['action'] == 'home') : ?>active<?php endif; ?>">
                                <a class="nav-link" style="color: white" id="home1" onmouseover="mouseOverHome1()"
                                   onmouseleave="mouseLeaveHome1()" href="index.php?action=home">Accueil</a>
                            </li>
                            <li class="nav-item
                        <?php if ($_GET['action'] == 'createRecipe') : ?>active<?php endif; ?>">
                                <a class="nav-link" style="color: white" id="createRecipe" onmouseover="mouseOverCreateRecipe()"
                                   onmouseleave="mouseLeaveCreateRecipe()" href="index.php?action=createRecipe">Créer recette</a>
                            </li>
                            <li class="nav-item
                        <?php if ($_GET['action'] == 'search') : ?>active<?php endif; ?>">
                                <a class="nav-link" style="color: white" id="search" onmouseover="mouseOverSearch()"
                                   onmouseleave="mouseLeaveSearch()" href="index.php?action=search">Rechercher</a>
                            </li>
                            <li class="nav-item dropdown <?php if ($_GET['action'] == 'profil') : ?>active<?php endif; ?>">
                                <a class="nav-link dropdown-toggle" style="color: white" onmouseover="mouseOverProfil()"
                                   onmouseleave="mouseLeaveProfil()" href="index.php?action=profil" id="profil" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Mon profil
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <?php if ( $_SESSION['admin'] == true):?>
                                        <a class="dropdown-item" id="confirmRecipes" onmouseover="mouseOverConfirmRecipes()" onmouseleave="mouseLeaveConfirmRecipes()" href="index.php?action=confirmRecipes">Valider recettes</a>
                                        <a class="dropdown-item" id="users" onmouseover="mouseOverUsers()" onmouseleave="mouseLeaveUsers()" href="index.php?action=users">Utilisateurs</a>
                                    <?php else:?>
                                        <a class="dropdown-item" id="savedPlates" onmouseover="mouseOverSavedPlates()" onmouseleave="mouseLeaveSavedPlates()" href="index.php?action=savedPlates">Plats sauvegardés</a>
                                    <?php endif;?>
                                </div>
                            </li>
                            <li class="nav-item
                        <?php if ($_GET['action'] == 'logout') : ?>active<?php endif; ?>">
                                <a class="nav-link" style="color: white" id="logout" onmouseover="mouseOverLogout()"
                                   onmouseleave="mouseLeaveLogout()" href="index.php?action=logout">Logout</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <?php if ($_GET['action'] == 'home') : ?>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="view/css/image/image0.jpeg" style="filter: brightness(90%)" class="d-block w-100"
                                 alt="50px">
                        </div>
                        <div class="carousel-item">
                            <img src="view/css/image/image1.jpeg" style="filter: brightness(60%)" class="d-block w-100"
                                 alt="150px">
                        </div>
                        <div class="carousel-item">
                            <img src="view/css/image/image2.jpeg" style="filter: brightness(60%)" class="d-block w-100"
                                 alt="50px">
                        </div>
                        <?php $useragent = $_SERVER['HTTP_USER_AGENT'];

                        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) : ?>
                        <?php else: ?>
                            <div class="carousel-caption text-center">
                                <a class="btn btn-outline-light btn-lg animated fadeInUp" href="#calculator">Calculer votre plat</a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    </a>
                </div>
            <?php endif; ?>
        </div>
        <div class="container">
        </div>
        <div class="contentArea">
            <div class="divPanel notop page-content">
                <div class="row-fluid">
                    <!--__________CONTENU__________-->
                    <div class="span12" id="divMain">
                        <?= $contenu; ?>
                    </div>
                    <!--________FIN CONTENU________-->
                </div>
            </div>
        </div>
        <div class="row" style="background-color: lightgrey; height: 375px;">
            <div class="col-md-4 my-auto text-center">
                <h5>FoodCalculator</h5><br>
                <label>Avenue de la Gare 14</label><br>
                <label>1450 Sainte-Croix</label><br>
                <label>Téléphone: 078 744 49 90</label><br>
                <label>E-Mail: dylan-david.cunha-rocha@cpnv.ch</label><br>
            </div>
            <div class="col-md-4 my-auto text-center">
                <img src="view/css/image/logo.png" class="align-middle" style="width: 80%;">
            </div>
            <div class="col-md-4 text-center my-auto">
                <a href="index.php?action=home" style="color: black">Accueil |</a>
                <a href="index.php?action=recette" style="color: black">Créer recette |</a>
                <a href="index.php?action=rechercher" style="color: black">Rechercher |</a>
                <a href="index.php?action=profil" style="color: black">Mon profil</a>
            </div>
        </div>
    </div>
</body>