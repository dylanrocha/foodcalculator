<?php

/**
 * This function is to get the recipes that are showed on the home page as recommendation.
 * @return array|false|null
 */
function getRecipes(){
    $recipesInfos = "select * from recipes where accepted = 1 ORDER BY ID DESC LIMIT 6;";
    $resultRecipesInfos = executeQuery($recipesInfos);

    return $resultRecipesInfos;
}

/**
 * This function is to show to the users the recipes with the name that he searched.
 * @param $searchRecipes is to get what the user typed in the search bar of the recipes.
 * @return bool
 */
function recipeSearched($searchRecipes){
    $recipesSearch = "select * from recipes where name like '" . $searchRecipes['searchRecipes'] . "%' and accepted = 1;";
    $resultSearchInfos = executeQuery($recipesSearch);

    $_GET['recipes'] = $resultSearchInfos;

    return true;
}

/**
 * This function is to display a recipe that the user clicked on.
 * @return bool
 */
function recipe(){
    $recipeSearch = "select * from recipes where name = '" . $_GET['name'] . "';";
    $_GET['recipe'] = executeQuery($recipeSearch);

    $recipeCreator = "select * from users where id = '" .  $_GET['recipe'][0]['users_id'] . "';";
    $_GET['recipeC'] = executeQuery($recipeCreator);

    $recipeParticularity = "select * from particularities where id = '" .  $_GET['recipe'][0]['particularities_id'] . "';";
    $_GET['recipeP'] = executeQuery($recipeParticularity);

    $recipeType = "select * from types where id = '" .  $_GET['recipe'][0]['types_id'] . "';";
    $_GET['recipeT'] = executeQuery($recipeType);

    $recipeIngredient = "SELECT ingredients.name, recipes_use_ingredients.quantities, recipes_use_ingredients.calories FROM ingredients INNER JOIN recipes_use_ingredients where ingredients.id = recipes_use_ingredients.ingredients_id AND recipes_use_ingredients.recipes_id = ".$_GET['recipe'][0]['id'].";";
    $_GET['recipeI'] = executeQuery($recipeIngredient);

    $nbCalories = count($_GET['recipeI']);
    $i = 0;
    $_GET['recipeCT'] = 0;

    while ($i < $nbCalories){
        $_GET['recipeCT'] = $_GET['recipeCT'] + $_GET['recipeI'][$i]['calories'];
        $i++;
    }

    return true;
}

/**
 * This functions adds to the database the recipe created by the user.
 * @param $recipeInfo is to collect all the ingredients with their grammes and calories and all the others infos about the recipe.
 * @return bool
 */
function addRecipe($recipeInfo){

    $date = getdate();
    $todayDate = "$date[year]-$date[mon]-$date[mday]";

    $recipeType = "select * from types where name = '".$recipeInfo['recipeType']."';";
    $resultRecipeType = executeQuery($recipeType);

    $recipeParticularities = "select * from particularities where name = '".$recipeInfo['recipeParticularity']."';";
    $resultRecipeParticularities = executeQuery($recipeParticularities);

    $addRecipe = "insert into recipes(name,preparation,picture,creationDate,difficulty,cookingTime,preparationTime,nbPeople,particularities_id,users_id,types_id,accepted) values ('".$recipeInfo['recipeName']."','".$recipeInfo['recipePreparation']."','view/css/image/image0.jpeg','".$todayDate."','".$recipeInfo['recipeDifficulty']."',".$recipeInfo['recipeCookingTime'].",".$recipeInfo['recipePreparationTime'].",".$recipeInfo['recipeNbPeople'].",".$resultRecipeParticularities[0]['id'].",".$_SESSION['user'][0]['id'].",".$resultRecipeType[0]['id'].",0);";
    executeQuery($addRecipe);

    $recipeID = "SELECT * FROM recipes ORDER BY ID DESC LIMIT 1;";
    $resultRecipeID = executeQuery($recipeID);

    if (isset($_SESSION['ingredientsAdded2'])){
        $nbIngredients = count($_SESSION['ingredientsAdded2']);
        $i = 0;
        while ($i < $nbIngredients) {
            $savePlateIngredients = "INSERT INTO recipes_use_ingredients(recipes_id,ingredients_id,quantities,calories) VALUES (" . $resultRecipeID[0]['id'] . "," . $_SESSION['ingredientsAdded2'][$i]['id'] . ",".$_SESSION['ingredientsAdded2'][$i]['quantities'].",".$_SESSION['ingredientsAdded2'][$i]['calories'].")";
            executeQuery($savePlateIngredients);
            $i++;
        }
        $_GET['noIngredients'] = false;
        unset($_SESSION['ingredientsAdded2']);
        unset($_SESSION['ingredients']);
        return true;
    }else{
        $_GET['noIngredients'] = true;
        return false;
    }
}

/**
 * This function is to get the recipes that need approval.
 * @return array|false|null
 */
function getRecipesAwaiting(){
        $recipesInfos = "select * from recipes where accepted = 0;";
        $resultRecipesInfos = executeQuery($recipesInfos);

        return $resultRecipesInfos;
}

/**
 * This function is to accept the recipes by the admin.
 * @return bool
 */
function acceptRecipes(){

    $acceptRecipe = "update recipes set accepted = 1 where id = ".$_GET['id'].";";
    executeQuery($acceptRecipe);

    return true;

}

/**
 * This function is to decline the recipes by the admin.
 * @return bool
 */
function declineRecipes(){

    $declineRecipe = "delete from recipes where id = ".$_GET['id'].";";
    executeQuery($declineRecipe);

    return true;

}