<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 11.02.2022
 * Time: 11:49
 */
$titre = "FoodCalculator - Register";
?>
<!DOCTYPE HTML>
<html>
<head>
    <link rel="icon" type="image/png" href="view/css/image/logo.png"/>
    <meta charset="utf-8">
    <title><?= $titre; ?></title>
    <link rel="stylesheet" href="view/css/style.css">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/mdb.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/style.css">
    <style>
        .img {
            background: url('view/css/image/image0.jpeg');
            background-size:cover;
            background-repeat:no-repeat;
        }

        html, body {
            margin: 0;
            height: 100%;
        }
    </style>
</head>
<body class="img">
    <script src="js/code.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/mdb.min.js"></script>

    <script>
        $(document).ready(function () {
            new WOW().init();
        });
    </script>

    <article>
        <form class="form" method="post" action="index.php?action=createRegister">
            <div class="bg container-fluid">
                <div class="row">
                    <div class="col-md-4" style="text-align: center">
                        <img class="mt-5" src="view/css/image/logo.jpg">
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <br><br><br><br>
                <p style="text-align: center; color: white; font-size: 40px"><b>Inscrivez-vous !</b></p><br>
                <?php if (isset($_GET['errorEP']) && $_GET['errorEP'] == true) :?>
                    <style>
                        #user, #pseudo{
                            border: 2px solid red;
                        }
                    </style>
                    <h5 style="color: red" align="center">E-mail ou pseudo déjà utilisés</h5>
                <?php endif;?>
                <?php if (isset($_GET['errorPass']) && $_GET['errorPass'] == true) :?>
                    <style>
                        #password, #password2{
                            border: 2px solid red;
                        }
                    </style>
                    <h5 style="color: red" align="center">Mot de passe différents</h5>
                <?php endif;?>
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4"">
                    <form style="color: white;">
                        <div class="form-group">
                            <label for="user" style="color: white"><b>Adresse email</b></label>
                            <input type="email" class="form-control" id="user" name="user" placeholder="Entrez email" required>
                        </div>
                        <div class="form-group">
                            <label for="pseudo" style="color: white"><b>Pseudo</b></label>
                            <input type="text" class="form-control" id="pseudo" name="pseudo" placeholder="Entrez pseudo" required>
                        </div>
                        <div class="form-group">
                            <label for="password" style="color: white"><b>Mot de passe</b></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Entrez mot de passe" required>
                        </div>
                        <div class="form-group">
                            <label for="password2" style="color: white"><b>Confirmez mot de passe</b></label>
                            <input type="password" class="form-control" id="password2" name="password2" placeholder="Re entrez mot de passe" required>
                        </div>
                        <div class="form-check">
                            <a href="index.php?action=login" style="color: white">Je possède déjà un compte</a>
                        </div>
                        <div align="right">
                            <input type="submit" value="Register" class="btn btn-black">
                            <button type="reset" class="btn btn-white">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </form>
    </article>
</body>
