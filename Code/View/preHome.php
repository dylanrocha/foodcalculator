<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.02.2022
 * Time: 14:50
 */
$titre = "FoodCalculator - Pre-Home";
?>
<!DOCTYPE HTML>
<html>
<head>
    <img rel="icon" type="image/png" href="view/css/image/logo.png"/>
    <meta charset="utf-8">
    <title><?= $titre; ?></title>
    <link rel="stylesheet" href="view/css/style.css">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/mdb.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/style.css">
    <style>
        .img {
            background: url('view/css/image/image0.jpeg');
            background-size:cover;
            background-repeat:no-repeat;
        }

        html, body {
            margin: 0;
            height: 100%;
        }
    </style>
</head>
<body class="img">
    <script src="js/code.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/mdb.min.js"></script>

    <script>
        $(document).ready(function () {
            new WOW().init();
        });
    </script>


    <div class="bg container-fluid">
        <div class="row">
            <div class="col-md-4" style="text-align: center">
                <img class="mt-5" src="view/css/image/logo.jpg">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <br><br><br><br><br><br><br><br><br>
        <p style="text-align: center; color: white; font-size: 40px"><b>Connectez-vous !</b></p><br>
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4" align="center">
                <a href="index.php?action=login"><button type="button" class="btn btn-black">
                    Connexion
                    </button></a>
                <a href="index.php?action=register"><button type="button" href="index.php?action=register" class="btn btn-black">
                    Créer un compte
                    </button></a>
            </div>
            <div class="col-md-4">

            </div>
    </div>
</body>

