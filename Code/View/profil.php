<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.02.2022
 * Time: 14:50
 */
ob_start();
$titre = "FoodCalculator - Mon profil";
?>
<?php if (isset($_SESSION['user'])):?>
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12 text-center" style="margin-top: 40px">
                <h1 class="display-4">Mon profil</h1>
            </div>
        </div>
    </div>
<?php elseif (empty($_SESSION['user'])):?>
    <?php prehome();?>
<?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
