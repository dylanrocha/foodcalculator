<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 10.03.2022
 * Time: 11:50
 */
ob_start();
$titre = "FoodCalculator - plats sauvegardés";
?>
    <style>
        .img {
            background: url('view/css/image/image0.jpeg');
            background-size:cover;
            background-repeat:no-repeat;
        }

        .fullPage {
            margin: 0;
            height: 100%;
        }

    </style>
<?php if (isset($_SESSION['user'])):?>
<div class="fullPage img">
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center" style="margin-top: 100px;color: white;">
                <table class="table" align="center" valign="middle">
                    <thead>
                        <tr style="color: white;" align="center" valign="middle">
                            <th scope="col">Plat</th>
                            <th scope="col">Quantité [g]</th>
                            <th scope="col">Calories [kcal]</th>
                            <th scope="col">
                                <select type="text" name="mois" class="rounded btn-outline-success" required>
                                    <option value="janvier">Janvier</option>
                                    <option value="février">Février</option>
                                    <option value="mars">Mars</option>
                                    <option value="avril">Avril</option>
                                    <option value="mai">Mai</option>
                                    <option value="juin">Juin</option>
                                    <option value="juillet">Juillet</option>
                                    <option value="août">Août</option>
                                    <option value="septembre">Septembre</option>
                                    <option value="octobre">Octobre</option>
                                    <option value="novembre">Novembre</option>
                                    <option value="décembre">Décembre</option>
                                </select>
                                <input type="button" class="btn btn-success btn-sm" id="min" name="min" value="+"><input type="button" class="btn btn-success btn-sm" id="max" value="-" name="max"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($_GET['platesFilled']):?>
                        <?php foreach ($_GET['userPlatesInfo'] as $userPlate):?>
                            <tr style="color: white;" align="center" valign="middle">
                                <td><?=$userPlate['name']?></td>
                                <td><?=$userPlate['quantities']?></td>
                                <td><?=$userPlate['calories']?></td>
                                <td><?=$userPlate['creationDate']?></td>
                            </tr>
                        <?php endforeach;?>
                    <?php else:?>
                        <tr>
                            <td><h5 style="color: red;">Pas de plats sauvegardés</h5></td>
                        </tr>
                    <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php elseif (empty($_SESSION['user'])):?>
    <?php prehome();?>
<?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";