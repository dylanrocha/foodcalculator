<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.02.2022
 * Time: 14:31
 */
require "controler/controler.php";

// Switch that will read the value of the action and execute the function in the controller, if no value then by default it will be the home function if the user is connected
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    switch ($action) {
        case 'prehome' :
            prehome();
            break;
        case 'home' :
            home();
            break;
        case 'login':
            login();
            break;
        case 'checkLogin' :
            isLoginCorrect($_POST);
            break;
        case 'logout':
            logout();
            break;
        case 'register':
            register();
            break;
        case 'createRegister':
            isRegisterCorrect($_POST);
            break;
        case 'createRecipe':
            createRecipe();
            break;
        case 'profil':
            profil();
            break;
        case 'search':
            search();
            break;
        case 'searchRecipes':
            searchRecipes($_POST);
            break;
        case 'recipes':
            recipes();
            break;
        case 'searchIngredients':
            searchIngredients($_POST);
            break;
        case 'searchIngredientsRecipe':
            searchIngredientsRecipe($_POST);
            break;
        case 'addIngredients':
            addIngredients($_POST);
            break;
        case 'addIngredientsRecipe':
            addIngredientsRecipe($_POST);
            break;
        case 'removeIngredients':
            removeIngredients($_POST);
            break;
        case 'removeIngredientsRecipe':
            removeIngredientsRecipe($_POST);
            break;
        case 'confirmRecipes':
            confirmRecipes();
            break;
        case 'savedPlates':
            savedPlates();
            break;
        case 'confirmPlate':
            confirmPlate($_POST);
            break;
        case 'users':
            users();
            break;
        case 'addRecipes':
            addRecipes($_POST);
            break;
        case 'acceptRecipe':
            acceptRecipe();
            break;
        case 'declineRecipe':
            declineRecipe();
            break;
        default:
            if (isset($_SESSION['user'])){
                home();
            }else{
                prehome();
            }
    }
} else {
    if (isset($_SESSION['user'])){
        home();
    }else{
        prehome();
    }
}
?>