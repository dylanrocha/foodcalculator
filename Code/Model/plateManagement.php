<?php

/**
 * This function adds to the the Data Base the plate created by the user.
 * @param $plateName takes all the informations that the users set about their plate.
 * @return bool
 */
function confirmPlates($plateName){

    $date = getdate();
    $todayDate = "$date[year]-$date[mon]-$date[mday]";

    $savePlate = "INSERT INTO plates(name,creationDate,users_id) VALUES ('". $plateName['plateName'] ."', '". $todayDate ."', ". $_SESSION['user'][0]['id'] .");";
    executeQuery($savePlate);

    $plateID = "SELECT * FROM plates ORDER BY ID DESC LIMIT 1;";
    $resultPlateID = executeQuery($plateID);

    $nbIngredients = count($_SESSION['ingredientsAdded']);
    $i = 0;
    while ($i < $nbIngredients) {
        $savePlateIngredients = "INSERT INTO ingredients_constitute_plates(plates_id,ingredients_id,quantities,calories) VALUES (" . $resultPlateID[0]['id'] . "," . $_SESSION['ingredientsAdded'][$i]['id'] . ",".$_SESSION['ingredientsAdded'][$i]['quantities'].",".$_SESSION['ingredientsAdded'][$i]['calories'].")";
        executeQuery($savePlateIngredients);
        $i++;
    }

    unset($_SESSION['ingredientsAdded']);
    unset($_SESSION['ingredients']);

    return true;
}

/**
 * This function is to get all the plates saved by the user and show to him.
 * @return bool
 */
function getPlates(){

        $getPlate2 = "SELECT plates.name, plates.creationDate, sum(ingredients_constitute_plates.quantities) as quantities, sum(ingredients_constitute_plates.calories) AS calories FROM ingredients_constitute_plates
                      right JOIN plates ON ingredients_constitute_plates.plates_id = plates.id AND plates.users_id =".$_SESSION['user'][0]['id']."
                      GROUP BY ingredients_constitute_plates.plates_id;";
        $_GET['userPlatesInfo'] = executeQuery($getPlate2);

    if (isset($_GET['userPlatesInfo'])){
        $_GET['platesFilled'] = true;
        return true;
    }else{
        $_GET['platesFilled'] = false;
        return false;
    }


}
;