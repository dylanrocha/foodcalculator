<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 10.03.2022
 * Time: 11:50
 */
ob_start();
$titre = "FoodCalculator - Valider recettes";
?>
<?php if (isset($_SESSION['user'])):?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center" style="margin-top: 150px;margin-bottom: 50px;">
                <h1 class="display-4">Valider recettes</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <table class="table" valign="middle">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Nom</th>
                            <th>Accepter</th>
                            <th>Décliner</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($recipesAwaiting as $recipes):?>
                        <tr>
                            <td><img src="<?= $recipes['picture']?>" style="border-radius: 4px;" width="150" height="100"></td>
                            <td><a href="index.php?action=recipes&name=<?= $recipes['name'];?>"><?= $recipes['name']?></a></td>
                            <td><a type="button" href="index.php?action=acceptRecipe&id=<?= $recipes['id'];?>" class="btn btn-light btn-sm">+</a></td>
                            <td><a class="btn btn-light btn-sm" type="button" href="index.php?action=declineRecipe&id=<?= $recipes['id'];?>">-</a></td>
                        </tr>
                    <?php endforeach?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php elseif (empty($_SESSION['user'])):?>
    <?php prehome();?>
<?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";