<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 11.02.2022
 * Time: 11:50
 */
ob_start();
$titre = "FoodCalculator - Créer recette";
?>
    <?php if (isset($_SESSION['user'])):?>
        <div class="container-fluid" style="margin-top: 150px;margin-bottom: 50px;">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1 class="display-4">Créer recette</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2" align="left">
                    <form action="index.php?action=addRecipes" method="post">
                        <?php if (isset($_GET['noIngredients'])) :?>
                            <?php if ($_GET['noIngredients'] == true):?>
                            <h5 style="color: red">Pas d'ingrédients ajoutés</h5>
                            <?php endif;?>
                        <?php endif;?>
                        Nom du plat :<input type="text" name="recipeName" class="form-control" required>
                        Préparation :<textarea type="text" name="recipePreparation" class="form-control" required></textarea>
                        Difficulté :
                        <select type="text" name="recipeDifficulty" class="form-control" required>
                            <option value="facile">Facile</option>
                            <option value="moyen">Moyen</option>
                            <option value="difficile">Difficile</option>
                        </select>
                        Particularité :<select type="text" name="recipeParticularity" class="form-control">
                            <option></option>
                            <option value="bio">Bio</option>
                            <option value="sans Gluten">Sans gluten</option>
                            <option value="sans Lactose">Sans lactose</option>
                            <option value="vegan">Vegan</option>
                        </select>
                        Temps de cuisson :<input type="number" name="recipeCookingTime" class="form-control" required>
                        Temps de préparation :<input type="number" name="recipePreparationTime" class="form-control" required>
                        Nombre de personnes :<input type="number" name="recipeNbPeople" class="form-control" required>
                        Type :
                        <select type="text" name="recipeType" class="form-control" required>
                            <option value="petit-déjeuner">Petit-déjeuner</option>
                            <option value="appéritif">Appéritif</option>
                            <option value="collation">Collation</option>
                            <option value="dessert">Dessert</option>
                            <option value="entrée">Entrée</option>
                            <option value="platPrincipal">Plat principal</option>
                        </select>
                        Photos :<input type="file" name="recipePicture" accept="image/*" class="form-control">
                        <input type="submit" class="btn btn-success mr-5" value="Confirmer">
                    </form>
                </div>
                <div class="col-md-5" align="center">
                    <form action="index.php?action=searchIngredientsRecipe" method="post" class="mt-3">
                        <div class="input-group" align="center">
                            <input type="search" class="form-control rounded mt-2" id="search" name="searchIngredientsRecipe" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                            <button type="submit" class="btn btn-outline-success">Rechercher</button>
                        </div>
                    </form>
                    <table class="table" align="center">
                        <thead align="center">
                        <tr align="center">
                            <th scope="col">Aliment</th>
                            <th scope="col">Quantité [g]</th>
                            <th scope="col">Calories [kcal]</th>
                            <th scope="col">Ajouter</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (isset($_SESSION['ingredients2'])):?>
                            <?php if ($_SESSION['ingredients2']):?>
                                <?php foreach ($_SESSION['ingredients2'] as $ingredient):?>
                                    <form action="index.php?action=addIngredientsRecipe" method="post">
                                        <tr align="center">
                                            <td style="display: none;" ><input value="<?= $ingredient['id']?>" name="addID"></td>
                                            <td style="vertical-align: middle" ><input value="<?= $ingredient['name']?>" name="addName" readonly></td>
                                            <td style="vertical-align: middle" ><input value="<?= $ingredient['quantities']?>" name="addQuantities"></td>
                                            <td style="vertical-align: middle" ><input value="<?= $ingredient['calories']?>" name="addCalories" readonly></td>
                                            <td style="vertical-align: middle"><button type="submit" class="btn btn-light btn-sm">+</button></td>
                                        </tr>
                                    </form>
                                <?php endforeach;?>
                            <?php else:?>
                                <tr>
                                    <td style="color: red">Aucun ingrédients trouvé</td>
                                </tr>
                            <?php endif;?>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-5">
                    <table class="table" align="center">
                        <thead align="center">
                        <tr align="center">
                            <th scope="col">Aliment</th>
                            <th scope="col">Quantité [g]</th>
                            <th scope="col">Calories [kcal]</th>
                            <th scope="col">Retirer du plat</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (isset($_SESSION['ingredientsAdded2'])):?>
                            <?php foreach ($_SESSION['ingredientsAdded2'] as $ingredients):?>
                                <form action="index.php?action=removeIngredientsRecipe" method="post">
                                    <tr align="center">
                                        <td style="display: none;"><input value="<?= $ingredient['id']?>" name="removeID"></td>
                                        <td style="vertical-align: middle" id="removeName"><input name="removeName" value="<?=$ingredients['name']?>" readonly></td>
                                        <td style="vertical-align: middle" id="removeQuantity"><input name="removeQuantities" value="<?=$ingredients['quantities']?>" readonly></td>
                                        <td style="vertical-align: middle" id="removeCalories"><input name="removeCalories" value="<?=$ingredients['calories']?>" readonly></td>
                                        <td style="vertical-align: middle"><button class="btn btn-light btn-sm" type="submit">-</button></td>
                                    </tr>
                                </form>
                            <?php endforeach;?>
                        <?php else:?>
                            <tr align="center">
                                <td style="vertical-align: middle" id="removeName"></td>
                                <td style="vertical-align: middle" id="removeQuantities"></td>
                                <td style="vertical-align: middle" id="removeCalories"></td>
                                <td style="vertical-align: middle"></td>
                            </tr>
                        <?php endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php elseif (empty($_SESSION['user'])):?>
        <?php prehome();?>
    <?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";