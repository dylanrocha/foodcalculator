<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.03.2022
 * Time: 15:05
 */

require "dbConnector.php";

/**
 * This function is to verify that the user is connecting with the right info, if yes then the user will be connected and send to home page.
 * @param $formL is to get the infos that were set by the user trying to connect.
 * @return bool
 */
function checkLogin($formL)
{
    $requete = "Select * from users where emailAddress = '" . $formL['user'] . "' OR pseudo = '" . $formL['user'] . "';";
    $requete2 = "Select emailAddress from users where emailAddress = '" . $formL['user'] . "' OR pseudo = '" . $formL['user'] . "';";
    $requete3 = "select emailAddress from users where emailAddress = '" . $formL['user'] . "' and roles_id = 2";

    $queryResult = executeQuery($requete);
    $queryResult2 = executeQuery($requete2);
    $queryResult3 = executeQuery($requete3);

    if ($queryResult3) {
        $_SESSION['admin'] = true;
        if ($queryResult) {
            $userHashedPassword = $queryResult[0]["password"];
            if (password_verify($formL['password'], $userHashedPassword)) {
                $_SESSION['user'][0]['emailAddress'] = $queryResult2[0]["emailAddress"];
                $_SESSION['user'][0]['id'] = $queryResult[0]['id'];
                $_GET['error'] = false;
                return true;
            } else {
                $_GET['error'] = true;
                return false;
            }
        } else {
            $_GET['error'] = true;
            $_SESSION['admin'] = false;
            return false;
        }
    }else{
        $_SESSION['admin'] = false;
        if ($queryResult) {
            $userHashedPassword = $queryResult[0]["password"];
            if (password_verify($formL['password'], $userHashedPassword)) {
                $_SESSION['user'][0]['emailAddress'] = $queryResult2[0]["emailAddress"];
                $_SESSION['user'][0]['id'] = $queryResult[0]['id'];
                $_GET['error'] = false;
                return true;
            } else {
                $_GET['error'] = true;
                return false;
            }
        } else {
            $_GET['error'] = true;
            return false;
        }
    }
}

/**
 * This function is to verify that the user is creating an account with the right infos, if yes then the user will be connected and send to home page and created in the database.
 * @param $formR is to get the infos that were set by the user trying to create an account.
 * @return bool
 */
function createAccount($formR)
{
    $requeteCheck = "Select emailAddress, pseudo from users where emailAddress ='" . $formR['user'] . "' or pseudo = '" . $formR['pseudo'] . "';";
    $queryResult = executeQuery($requeteCheck);

    if ($queryResult) {
        $_GET['errorEP'] = true;
        return false;
    } else {
        $_GET['errorEP'] = false;
        if ($formR['password'] == $formR['password2']) {
            $passHash = password_hash($formR['password'], PASSWORD_DEFAULT);
            $requeteCreate = "INSERT INTO users (emailAddress, password, pseudo, roles_id) VALUES ('" . $formR['user'] . "','" . $passHash . "','" . $formR['pseudo'] . "','" . 1 ."');";
            executeQuery($requeteCreate);

            $_SESSION['user'][0]['emailAddress'] = $formR['user'];
            $_GET['errorPass'] = false;
            return true;
        } else {
            $_GET['errorPass'] = true;
            return false;
        }
    }
}

