<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 11.02.2022
 * Time: 10:56
 */
$titre = "FoodCalculator - Login";
?>
<!DOCTYPE HTML>
<html>
<head>
    <link rel="icon" type="image/png" href="view/css/image/logo.png"/>
    <meta charset="utf-8">
    <title><?= $titre; ?></title>
    <link rel="stylesheet" href="view/css/style.css">
    <meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/mdb.min.css">
    <link rel="stylesheet" href="view/css/mdbootstrap/css/style.css">
    <style>
        .img {
            background: url('view/css/image/image0.jpeg');
            background-size:cover;
            background-repeat:no-repeat;
        }

        html, body {
            margin: 0;
            height: 100%;
        }
    </style>
</head>
<body class="img">
    <script src="js/code.js"></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js'></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/jquery.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/popper.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="view/css/mdbootstrap/js/mdb.min.js"></script>

    <script>
        $(document).ready(function () {
            new WOW().init();
        });
    </script>


    <div class="bg">
        <div class="row">
            <div class="col-md-4" style="text-align: center">
                <img class="mt-5" src="view/css/image/logo.jpg">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
        </div>
        <br><br><br><br><br><br><br>
        <p style="text-align: center; color: white; font-size: 40px"><b>Connectez-vous !</b></p><br>
        <?php if (isset($_GET['error']) && $_GET['error'] == true) :?>
            <h5 style="text-align: center; color: red">Identifiants erronés</h5>
        <?php endif;?>
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4"">
                <article>
                    <form class="form" method="post" style=" color: white;" action="index.php?action=checkLogin">
                        <div class="form-group">
                            <label for="user" style="color: white"><b>Email address</b></label>
                            <input type="email" class="form-control" id="user" name="user" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="password" style="color: white"><b>Password</b></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                        <div class="form-check">
                            <div align="left">
                                <a style="color: white">Mot de passe oublié</a><br>
                                <a href="index.php?action=register" style="color: white">Créer un compte</a>
                            </div>
                            <div align="right">
                                <input type="submit" value="Login" class="btn btn-black">
                                <button type="reset" class="btn btn-white">Reset</button>
                            </div>
                        </div>
                    </form>
                </article>
            </div>
        </div>
        <div class="col-md-4">

        </div>
    </div>
</body>
