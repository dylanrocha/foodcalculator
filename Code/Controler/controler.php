<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.02.2022
 * Time: 14:39
 */
session_start();

require "model/userManagement.php";
require "model/recipeManagement.php";
require "model/ingredientManagement.php";
require "model/plateManagement.php";

/**
 * @return void
 */
function prehome(){
    $_GET['action'] = 'prehome';
    require "view/preHome.php";
}

/**
 * @return void
 */
function home(){
    $_GET['action'] = 'home';
    $recipes = getRecipes();
    require "view/home.php";
}

/**
 * @return void
 */
function login(){
    $_GET['action'] = 'login';
    require "view/login.php";
}

/**
 * @return void
 */
function register(){
    $_GET['action'] = 'register';
    require "view/register.php";
}

/**
 * @return void
 */
function createRecipe(){
    $_GET['action'] = 'recette';
    require "view/createRecipe.php";
}

/**
 * @return void
 */
function search(){
    $_GET['action'] = 'search';
    require "view/search.php";
}


/**
 * @return void
 */
function recipes(){
    if (recipe()){
        $_GET['action'] = 'recipes';
        require "view/recipe.php";
    }
}

/**
 * @param $searchRecipes
 * @return void
 */
function searchRecipes($searchRecipes){
    if (recipeSearched($searchRecipes)){
        search();
    }
}

/**
 * @return void
 */
function profil(){
    $_GET['action'] = 'profil';
    require "view/profil.php";
}

/**
 * @param $formR
 * @return void
 */
function isRegisterCorrect($formR)
{
    if (createAccount($formR)) {
        home();
    } else {
        register();
    }
}

/**
 * @param $formL
 * @return void
 */
function isLoginCorrect($formL)
{
    if (checkLogin($formL)) {
        home();
    } else {
        login();
    }
}

/**
 * @return void
 */
function logout()
{
    session_destroy();
    prehome();
}

/**
 * @return void
 */
function confirmRecipes(){
    $_GET['action'] = 'confirmRecipes';
    $recipesAwaiting = getRecipesAwaiting();
    require "view/confirmRecipes.php";
}

/**
 * @return void
 */
function users(){
    $_GET['action'] = 'users';
    require "view/users.php";
}

/**
 * @return void
 */
function savedPlates(){
    if (getPlates()){
        $_GET['action'] = 'savedPlates';
        require "view/savedPlates.php";
    }else{
        $_GET['action'] = 'savedPlates';
        require "view/savedPlates.php";
    }

}

/**
 * @param $ingredientsR
 * @return void
 */
function searchIngredients($ingredientsR){
    if (getIngredients($ingredientsR)){
        home();
    }else{
        home();
    }
}

/**
 * @param $ingredientsR
 * @return void
 */
function searchIngredientsRecipe($ingredientsR){
    if (getIngredientsRecipe($ingredientsR)){
        createRecipe();
    }else{
        createRecipe();
    }
}

/**
 * @param $addIngredients
 * @return void
 */
function addIngredients($addIngredients){
        if (addIngredient($addIngredients)){
            home();
        }
}

/**
 * @param $addIngredients
 * @return void
 */
function addIngredientsRecipe($addIngredients){
    if (addIngredientRecipe($addIngredients)){
        createRecipe();
    }
}

/**
 * @param $removeIngredients
 * @return void
 */
function removeIngredients($removeIngredients){
    if (removeIngredient($removeIngredients)){
        home();
    }
}

/**
 * @param $removeIngredients
 * @return void
 */
function removeIngredientsRecipe($removeIngredients){
    if (removeIngredientRecipe($removeIngredients)){
        createRecipe();
    }
}

/**
 * @param $plateName
 * @return void
 */
function confirmPlate($plateName){

    if (confirmPlates($plateName)){
        savedPlates();
    }

}

/**
 * @param $recipeInfo
 * @return void
 */
function addRecipes($recipeInfo){

    if (addRecipe($recipeInfo)){
        home();
    }else{
        createRecipe();
    }

}

/**
 * @return void
 */
function acceptRecipe(){

    if (acceptRecipes()){
        confirmRecipes();
    }

}

/**
 * @return void
 */
function declineRecipe(){

    if (declineRecipes()){
        confirmRecipes();
    }

}