<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 11.02.2022
 * Time: 11:56
 */
ob_start();
$titre = "FoodCalculator - Rechercher";
?>
    <style>
        .img {
            background: url('view/css/image/image0.jpeg');
            background-size:cover;
            background-repeat:no-repeat;
        }

        .fullPage {
            margin: 0;
            height: 100%;
        }

    </style>
<?php if (isset($_SESSION['user'])):?>
<div class="img fullPage">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-lg-6" align="center" style="margin-top: 100px; color: white">
            <h1 class="display-4">Rechercher</h1>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" align="center">
                <form action="index.php?action=searchRecipes" method="post">
                    <div class="input-group" align="center">
                        <input type="search" class="form-control rounded mt-2" id="search" name="searchRecipes" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                        <button type="submit" href="#calculator"v class="btn btn-success">Rechercher</button>
                    </div>
                </form>
            </div>
        </div>
        <?php if (isset($_GET['recipes'])):?>
            <div class="row">
                <div class="col-md-12" align="left" style="background-color: white;">
                    <table style="margin-top: 10px; margin-bottom: 10px;">
                        <tbody>
                            <?php foreach ($_GET['recipes'] as $recipe):?>
                                    <tr align="center" onmouseover="mouseOverRecipe()" onmouseleave="mouseLeaveRecipe()" id="recipe" style="cursor: pointer;">
                                        <td style="vertical-align: middle"><img style="border-radius: 4px;width: 150px;" src="<?= $recipe['picture']?>"></th>
                                        <td style="vertical-align: middle; font-size: 30px; font-family: 'Century Gothic';margin-left: 5px;"><a style="color: black" href="index.php?action=recipes&name=<?= $recipe['name'];?>"><?= $recipe['name']?></a></td>
                                    </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php endif;?>
    </div>
</div>

<?php elseif (empty($_SESSION['user'])):?>
    <?php prehome();?>
<?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
