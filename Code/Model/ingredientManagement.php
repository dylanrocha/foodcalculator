<?php

/**
 * This function is to display all the ingredients that start with the letters set by the user.
 * @param $ingredientsR is to get the word that was set in the search bar.
 * @return bool
 */
function getIngredients($ingredientsR){
    $ingredients = "select * from ingredients where name like '" . $ingredientsR['searchIngredients'] . "%';";
    $_SESSION['ingredients'] = executeQuery($ingredients);


    if ($_SESSION['ingredients']){
        return true;
    }else{
        return false;
    }
}

function getIngredientsRecipe($ingredientsR){
    $ingredients = "select * from ingredients where name like '" . $ingredientsR['searchIngredientsRecipe'] . "%';";
    $_SESSION['ingredients2'] = executeQuery($ingredients);


    if ($_SESSION['ingredients2']){
        return true;
    }else{
        return false;
    }
}

/**
 * This function is to add an ingredient to the list of the plate.
 * @param $addIngredients to collect the info of which ingredient and how much grammes and calories.
 * @return bool
 */
function addIngredient($addIngredients){
    if (isset($_SESSION['ingredientsAdded'])) {
        $nbIngredients = count($_SESSION['ingredientsAdded']);
        $i = 0;
        while ($i < $nbIngredients){
            if ($_SESSION['ingredientsAdded'][$i]['name'] == $addIngredients['addName']) {
                $y = $_SESSION['ingredientsAdded'][$i]['quantities'];
                $_SESSION['ingredientsAdded'][$i]['quantities'] = $_SESSION['ingredientsAdded'][$i]['quantities'] + $addIngredients['addQuantities'];
                $_SESSION['ingredientsAdded'][$i]['calories'] = ($_SESSION['ingredientsAdded'][$i]['calories'] / $y) * $_SESSION['ingredientsAdded'][$i]['quantities'];
                return true;
            }
            $i++;
        }
        $_SESSION['ingredientsAdded'][$nbIngredients]['id'] = $addIngredients['addID'];
        $_SESSION['ingredientsAdded'][$nbIngredients]['name'] = $addIngredients['addName'];
        $_SESSION['ingredientsAdded'][$nbIngredients]['quantities'] = $addIngredients['addQuantities'];
        $_SESSION['ingredientsAdded'][$nbIngredients]['calories'] = ($addIngredients['addCalories'] / 100) * $addIngredients['addQuantities'];
    }else{
        $nbIngredients = 0;
        $_SESSION['ingredientsAdded'][$nbIngredients]['id'] = $addIngredients['addID'];
        $_SESSION['ingredientsAdded'][$nbIngredients]['name'] = $addIngredients['addName'];
        $_SESSION['ingredientsAdded'][$nbIngredients]['quantities'] = $addIngredients['addQuantities'];
        $_SESSION['ingredientsAdded'][$nbIngredients]['calories'] = ($addIngredients['addCalories'] / 100) * $addIngredients['addQuantities'];
    }
    return true;
}

/**
 * This function is to add an ingredient to the list of the recipe.
 * @param $addIngredients to collect the info of which ingredient and how much grammes and calories.
 * @return bool
 */
function addIngredientRecipe($addIngredients){
    if (isset($_SESSION['ingredientsAdded2'])) {
        $nbIngredients = count($_SESSION['ingredientsAdded2']);
        $i = 0;
        while ($i < $nbIngredients){
            if ($_SESSION['ingredientsAdded2'][$i]['name'] == $addIngredients['addName']) {
                $y = $_SESSION['ingredientsAdded2'][$i]['quantities'];
                $_SESSION['ingredientsAdded2'][$i]['quantities'] = $_SESSION['ingredientsAdded2'][$i]['quantities'] + $addIngredients['addQuantities'];
                $_SESSION['ingredientsAdded2'][$i]['calories'] = ($_SESSION['ingredientsAdded2'][$i]['calories'] / $y) * $_SESSION['ingredientsAdded2'][$i]['quantities'];
                return true;
            }
            $i++;
        }
        $_SESSION['ingredientsAdded2'][$nbIngredients]['id'] = $addIngredients['addID'];
        $_SESSION['ingredientsAdded2'][$nbIngredients]['name'] = $addIngredients['addName'];
        $_SESSION['ingredientsAdded2'][$nbIngredients]['quantities'] = $addIngredients['addQuantities'];
        $_SESSION['ingredientsAdded2'][$nbIngredients]['calories'] = ($addIngredients['addCalories'] / 100) * $addIngredients['addQuantities'];
    }else{
        $nbIngredients = 0;
        $_SESSION['ingredientsAdded2'][$nbIngredients]['id'] = $addIngredients['addID'];
        $_SESSION['ingredientsAdded2'][$nbIngredients]['name'] = $addIngredients['addName'];
        $_SESSION['ingredientsAdded2'][$nbIngredients]['quantities'] = $addIngredients['addQuantities'];
        $_SESSION['ingredientsAdded2'][$nbIngredients]['calories'] = ($addIngredients['addCalories'] / 100) * $addIngredients['addQuantities'];
    }
    return true;
}

/**
 * This function is to remove one ingredient of the list of the plate created by the user.
 * @param $removeIngredients to collect which ingredients needs to be removed.
 * @return bool|void
 */
function removeIngredient($removeIngredients){
    $nbIngredients = count($_SESSION['ingredientsAdded']);
    $i = 0;
    while ($i < $nbIngredients){
        if ($_SESSION['ingredientsAdded'][$i]['name'] == $removeIngredients['removeName']) {
            unset($_SESSION['ingredientsAdded'][$i]);
            return true;
        }
        $i++;
    }
}

/**
 * This function is to remove one ingredient of the list of the recipe created by the user.
 * @param $removeIngredients to collect which ingredients needs to be removed.
 * @return bool|void
 */
function removeIngredientRecipe($removeIngredients){
    $nbIngredients = count($_SESSION['ingredientsAdded2']);
    $i = 0;
    while ($i < $nbIngredients){
        if ($_SESSION['ingredientsAdded2'][$i]['name'] == $removeIngredients['removeName']) {
            unset($_SESSION['ingredientsAdded2'][$i]);
            return true;
        }
        $i++;
    }
}

