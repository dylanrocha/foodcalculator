<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 18.03.2022
 * Time: 14:50
 */
ob_start();
$titre = "FoodCalculator - " . $_GET['recipe'][0]['name'];
?>
<?php if (isset($_SESSION['user'])):?>
    <div style="margin-top: 150px;margin-bottom: 50px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4" align="center">
                    <img src="<?= $_GET['recipe'][0]['picture']?>"  style="border-radius: 4px;" width="500" height="350">
                </div>
                <div class="col-lg-4" align="left">
                    <h1><?=$_GET['recipe'][0]['name']?></h1>
                    <p><h5 class="font-weight-bold">Préparation :</h5><?=$_GET['recipe'][0]['preparation']?></p>
                    <p><b class="font-weight-bold">Particularité :</b> <?=$_GET['recipeP'][0]['name']?></p>
                    <p><b class="font-weight-bold">Temps de cuisson :</b> <?=$_GET['recipe'][0]['cookingTime']?></p>
                    <p><b class="font-weight-bold">Temps de préparation :</b> <?=$_GET['recipe'][0]['preparationTime']?></p>
                    <p><b class="font-weight-bold">Nombre de personnes :</b> <?=$_GET['recipe'][0]['nbPeople']?></p>
                    <p><b class="font-weight-bold">Type :</b> <?=$_GET['recipeT'][0]['name']?></p>
                    <p><b class="font-weight-bold">Créer le :</b> <?=$_GET['recipe'][0]['creationDate']?></p>
                    <p><b class="font-weight-bold">Créer par :</b> <?=$_GET['recipeC'][0]['pseudo']?></p>
                </div>
                <div class="col-lg-4" align="">
                    <h1>Ingrédients</h1>
                    <?php foreach ($_GET['recipeI'] as $ingredients):?>
                        <p><b class="font-weight-bold"><?=$ingredients['name']?> :</b> <?= $ingredients['quantities']?> [g]</p>
                    <?php endforeach;?>
                    <p><b class="font-weight-bold">Calories totales :</b> <?= $_GET['recipeCT'] ?></p>
                </div>
            </div>
        </div>
    </div>
<?php elseif (empty($_SESSION['user'])):?>
    <?php prehome();?>
<?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";