<?php
/**
 * Created by PhpStorm.
 * User: Dylan-David.CUNHA-RO
 * Date: 07.02.2022
 * Time: 14:50
 */
ob_start();
$titre = "FoodCalculator - Accueil";
?>
<?php if (isset($_SESSION['user'])):?>
    <div class="row">
        <div class="col-md-12" align="center">
            <h3><b>Recettes recommandées !</b></h3>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php foreach ($recipes as $recipe):?>
                <div class="col-md-2" style="cursor: pointer;">
                    <img src="<?= $recipe['picture']; ?>" style="border-radius: 4px;" width="150" height="100">
                    <div class="top-left" style="position: absolute;top: 8px; left: 16px;color: white; text-shadow:  1px 0 0 #000000, -1px 0 0 #000000, 0 1px 0 #000000, 0 -1px 0 #000000, 1px 1px #000000, -1px -1px 0 #000000, 1px -1px 0 #000000, -1px 1px 0 #000000;"><a style="color: white" href="index.php?action=recipes&name=<?= $recipe['name'];?>"><?= $recipe['name']; ?></a></div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
    <div id="calculator">
        <div class="row">
            <div class="col-md-4 mt-5">

            </div>
            <div class="col-md-4" >
                <p style="text-align: center; color: black; font-size: 45px" align="center"><b>Calculez votre plat !</b></p><br>
            </div>
            <div class="col-md-4">

            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <form action="index.php?action=searchIngredients#calculator" method="post">
                        <div class="input-group" align="center">
                            <input type="search" class="form-control rounded mt-2" id="search" name="searchIngredients" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                            <button type="submit" href="#calculator"v class="btn btn-outline-success">Rechercher</button>
                        </div>
                    </form>
                    <table class="table" align="center">
                        <thead align="center">
                            <tr align="center">
                                <th scope="col">Aliment</th>
                                <th scope="col">Quantité [g]</th>
                                <th scope="col">Calories</th>
                                <th scope="col">Ajouter au plat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($_SESSION['ingredients'])):?>
                                <?php if ($_SESSION['ingredients']):?>
                                    <?php foreach ($_SESSION['ingredients'] as $ingredient):?>
                                        <form action="index.php?action=addIngredients#calculator" method="post">
                                            <tr align="center">
                                                <td style="display: none;"><input value="<?= $ingredient['id']?>" name="addID"></td>
                                                <td style="vertical-align: middle" ><input value="<?= $ingredient['name']?>" name="addName" readonly></td>
                                                <td style="vertical-align: middle" ><input type="number" value="<?= $ingredient['quantities']?>" name="addQuantities"></td>
                                                <td style="vertical-align: middle" ><input value="<?= $ingredient['calories']?>" name="addCalories" readonly></td>
                                                <td style="vertical-align: middle"><button type="submit" class="btn btn-light btn-sm">+</button></td>
                                            </tr>
                                        </form>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <tr>
                                        <td style="color: red">Aucun ingrédients trouvé</td>
                                    </tr>
                                <?php endif;?>
                            <?php endif;?>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-md-12"
                            <p style="text-align: left; color: black; font-size: 20px"><b>Plat :</b></p>
                            <table class="table" align="center">
                                <thead align="center">
                                    <tr align="center">
                                        <th scope="col">Aliment</th>
                                        <th scope="col">Quantité [g]</th>
                                        <th scope="col">Calories</th>
                                        <th scope="col">Retirer du plat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if (isset($_SESSION['ingredientsAdded'])):?>
                                        <?php foreach ($_SESSION['ingredientsAdded'] as $ingredients):?>
                                        <form action="index.php?action=removeIngredients#calculator" method="post">
                                            <tr align="center">
                                                <td style="display: none;"><input value="<?= $ingredient['id']?>" name="removeID"></td>
                                                <td style="vertical-align: middle" id="removeName"><input name="removeName" value="<?=$ingredients['name']?>" readonly></td>
                                                <td style="vertical-align: middle" id="removeQuantities"><input name="removeQuantities" value="<?=$ingredients['quantities']?>" readonly></td>
                                                <td style="vertical-align: middle" id="removeCalories"><input name="removeCalories" value="<?=$ingredients['calories']?>" readonly></td>
                                                <td style="vertical-align: middle"><button class="btn btn-light btn-sm" type="submit">-</button></td>
                                            </tr>
                                        </form>
                                        <?php endforeach;?>
                                    <?php else:?>
                                        <tr align="center">
                                            <td style="vertical-align: middle" id="removeName"></td>
                                            <td style="vertical-align: middle" id="removeQuantities"></td>
                                            <td style="vertical-align: middle" id="removeCalories"></td>
                                            <td style="vertical-align: middle"></td>
                                        </tr>
                                    <?php endif;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php if (isset($_SESSION['ingredientsAdded'])):?>
                    <div align="right">
                        <form action="index.php?action=confirmPlate" method="post">
                            <input name="plateName" placeholder="Nom du plat" class="btn-outline-success" required>
                            <button type="submit" class="btn btn-success">Sauvegarder</button>
                        </form>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
<?php elseif (empty($_SESSION['user'])):?>
    <?php prehome();?>
<?php endif?>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
